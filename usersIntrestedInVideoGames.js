const userData = require("./data");

function usersInterestedInVideoGames(userData, givenInterest = "Video Games") {
  let result = [];
  if (typeof userData === "object") {
    for (let user in userData) {
      if (userData[user]["interests"]) {
        // checks the useres interests with key "interests"
        let interests = userData[user]["interests"][0];
        if (interests.includes(givenInterest)) {
          result.push(user);
        }
      } else if (userData[user]["interest"]) {
        // checks the useres interests with key "interest"
        let interests = userData[user]["interest"][0];
        if (interests.includes(givenInterest)) {
          result.push(user);
        }
      }
    }
  }
  return result;
}

console.log(usersInterestedInVideoGames(userData, "Video Games"));
