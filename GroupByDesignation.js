const userData = require("./data");

let langs = ["Golang", "Javascript", "Python", "C", "C++", "Java", "D.net", "R"];


// supporting function to get languages of a users
function getLanguage(userObject) {
  for (let lang = 0; lang < langs.length; lang++) {
    if (userObject["desgination"].includes(langs[lang])) {
      return langs[lang];
    }
  }
  return "";
}


// Actual function
function desginationGroups(userData) {
  group = {};
  if (typeof userData === "object") {
    for (let user in userData) {
    let lang = getLanguage(userData[user]);
    //   group[lang] = user;
    if(group[lang]){
        group[lang].push(user);
    }else{
        group[lang] = [user];
    }
    }
  }
  return group;
}
console.log(desginationGroups(userData));
