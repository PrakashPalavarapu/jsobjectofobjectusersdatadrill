const userData = require("./data");

function getGermanUsers(userData, country="Germany") {
  let germanUsers = [];
  if (typeof userData === "object") {
    for (let user in userData) {
      if (userData[user]["nationality"] === country) {
        germanUsers.push(user);
      }
    }
  }
  return germanUsers;
}
console.log(getGermanUsers(userData,"Germany"));
